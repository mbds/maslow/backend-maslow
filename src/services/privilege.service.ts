import { repository, Filter } from '@loopback/repository';
import { PrivilegeRepository } from '../repositories/privilege.repository';

export class MyPrivilegeService {
  constructor(
    @repository(PrivilegeRepository) public privilegeRepository: PrivilegeRepository,
  ) { }

}
