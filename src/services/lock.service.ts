export interface LockService {
  getLockInfos(lockId: string): Promise<any>;
  openLock(lockId: string): Promise<any>;
  closeLock(lockId: string): Promise<any>;
}
