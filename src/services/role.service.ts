import { repository } from '@loopback/repository';
import { RoleRepository } from '../repositories';

export class MyRoleService {
  constructor(
    @repository(RoleRepository) public roleRepository: RoleRepository,
  ) { }

}
