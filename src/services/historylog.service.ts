import { repository } from '@loopback/repository';
import { HistoryLogRepository } from '../repositories';

export class MyHistoyLogService {
  constructor(
    @repository(HistoryLogRepository) public historyLogRepository: HistoryLogRepository,
  ) { }

}
