import { BootMixin } from '@loopback/boot';
import { ApplicationConfig, BindingKey } from '@loopback/core';
import { RestExplorerBindings, RestExplorerComponent } from '@loopback/rest-explorer';
import { RepositoryMixin, SchemaMigrationOptions } from '@loopback/repository';
import { RestApplication } from '@loopback/rest';
import { ServiceMixin } from '@loopback/service-proxy';
import * as path from 'path';
import { MyAuthenticationSequence } from './sequence';
import { UserRepository, RoleRepository, LockRepository, ThingAccessRepository, ThingAccessPrivilegesRepository, HistoryLogRepository } from './repositories';
import { JWTAuthenticationStrategy } from './strategies/jwt.strategy';
import { SECURITY_SCHEME_SPEC } from './utils/security.specs';
import { TokenServiceBindings, TokenServiceConstants, PasswordHasherBindings, UserServiceBindings } from './keys';
import { JWTService } from './services/jwt.service';
import { AuthenticationComponent, registerAuthenticationStrategy } from '@loopback/authentication';
import { BcryptHasher, PasswordHasher } from './services/hash-password.service';
import { MyUserService } from './services/user.service';
import { AuthorizationOptions, AuthorizationComponent, AuthorizationTags, AuthorizationDecision, AuthorizationBindings } from '@loopback/authorization';
import { RoleAuthorizationProvider } from './providers/role-authorization.provider';
import { PrivilegeRepository } from './repositories/privilege.repository';
import { HistoryLog } from './models';


/**
 * Information from package.json
 */
export interface PackageInfo {
  name: string;
  version: string;
  description: string;
}
export const PackageKey = BindingKey.create<PackageInfo>('application.package');

const pkg: PackageInfo = require('../package.json');

export class BackendMaslow extends BootMixin(ServiceMixin(RepositoryMixin(RestApplication))) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MyAuthenticationSequence);
    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.bind(RestExplorerBindings.CONFIG).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.component(AuthenticationComponent);
    registerAuthenticationStrategy(this, JWTAuthenticationStrategy);

    this.setUpBindings();
    this.api({
      openapi: '3.0.0',
      info: { title: pkg.name, version: pkg.version },
      paths: {},
      components: { securitySchemes: SECURITY_SCHEME_SPEC },
      servers: [{ url: '/' }]
    });

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  async migrateSchema(options?: SchemaMigrationOptions) {
    await super.migrateSchema(options);

    const userRepository: UserRepository = await this.getRepository(UserRepository);
    const privilegeRepository: PrivilegeRepository = await this.getRepository(PrivilegeRepository);
    const roleRepository: RoleRepository = await this.getRepository(RoleRepository);
    const lockRepository: LockRepository = await this.getRepository(LockRepository);
    const thingAccessRepository: ThingAccessRepository = await this.getRepository(ThingAccessRepository);
    const historyLogRepository: HistoryLogRepository = await this.getRepository(HistoryLogRepository);
    const thingAccessPrivilegesRepository: ThingAccessPrivilegesRepository = await this.getRepository(ThingAccessPrivilegesRepository);
    const passwordHasher: PasswordHasher =
      await this.get(PasswordHasherBindings.PASSWORD_HASHER);

    //Creation des privileges de base

    try {
      const privilege1 = await privilegeRepository.create({ id: 1, name: 'ouvrir porte', action: "OPEN", description: "une description de ça" });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }
    try {
      const privilege2 = await privilegeRepository.create({ id: 2, name: 'fermer porte', action: "CLOSE", description: "une description de ça" });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }
    // Creation des roles de base

    try {
      await roleRepository.create({ id: 'ADMIN', name: 'administrateur', description: 'Admin' });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }
    try {
      await roleRepository.create({ id: 'ENTERPRISE', name: 'entreprise', description: 'Compte entreprise' });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }
    try {
      await roleRepository.create({ id: 'USER', name: 'utilisateur', description: 'Simple user' });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }
    try {
      await roleRepository.create({ id: 'EMERGENCY', name: 'urgence', description: 'Emergency user' });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }


    // Creation Compte admin de test
    try {
      const user = await userRepository.create({
        id: 1,
        email: 'admin@maslow.mbds.fr',
        firstname: 'ADMIN',
        lastname: 'MASLOW',
        password: await passwordHasher.hashPassword('adminpassword'),
        creationDate: "2020-04-10T20:51:10.102Z",
        modificationDate: "2020-04-10T20:51:10.102Z",
        roleId: 'ADMIN'
      });
      //await userRoleRepository.create({ userId: user.id, roleId: 'ADMIN' });
      let thingAccessCreated = await thingAccessRepository.create({
        userId: user.id,
        thing_uid: 'lifx:whitelight:D073D5566A1A',
        start_date: '2020-03-29T20:51:10.102Z',
        end_date: '2020-04-10T20:51:10.102Z',
        periodic: true,
        periodicity: 'hebdomadaire',
        time: ({ start_hour: "08:00", end_hour: "10:11" }),

      })
      const privilege13 = await privilegeRepository.create({ id: 5, name: 'test', action: "blahblah", description: "un test de ça" });
      //thingAccessRepository.privileges(thingAccessCreated.id).create(privilege13);
      const res = await thingAccessPrivilegesRepository.create({ thingAccessId: thingAccessCreated.id, privilegeId: privilege13.id });
      console.log(res);

    } catch (error) {
      console.log('Error while migrating : ' + error);
    }

    // Creation Compte user de test
    try {
      const user = await userRepository.create({
        id: 2,
        email: 'user@maslow.mbds.fr',
        firstname: 'USER',
        lastname: 'MASLOW',
        password: await passwordHasher.hashPassword('userpassword'),
        creationDate: "2020-04-10T20:51:10.102Z",
        modificationDate: "2020-04-10T20:51:10.102Z",
        roleId: 'USER'
      });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }

    // Creation Compte d'urgence de test
    try {
      const user = await userRepository.create({
        id: 3,
        email: 'emergency@maslow.mbds.fr',
        firstname: 'EMERGENCY',
        lastname: 'MASLOW',
        password: await passwordHasher.hashPassword('emergencypassword'),
        creationDate: "2020-04-10T20:51:10.102Z",
        modificationDate: "2020-04-10T20:51:10.102Z",
        roleId: 'EMERGENCY'
      });
      //await userRoleRepository.create({ userId: user.id, roleId: 'EMERGENCY' });
      await thingAccessRepository.create({
        userId: user.id,
        thing_uid: 'lifx:whitelight:D073D5566A1A',
        start_date: '2020-03-29T20:51:10.102Z',
        end_date: '2020-04-10T20:51:10.102Z',
      })

    } catch (error) {
      console.log('Error while migrating : ' + error);
    }

    //Ajout de la serrure
    try {
      await lockRepository.create({ brand: "TheKeys", constructor_lock_id: "7619" });
    } catch (error) {
      console.log('Error while migrating : ' + error);
    }
  }

  setUpBindings(): void {
    this.bind(PackageKey).to(pkg);
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(TokenServiceConstants.TOKEN_SECRET_VALUE);
    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE);
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);

    // Bind bcrypt hash services
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);
    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService);

    //Authorization
    const authOptions: AuthorizationOptions = {
      precedence: AuthorizationDecision.DENY,
      defaultDecision: AuthorizationDecision.DENY,
    };
    this.configure(AuthorizationBindings.COMPONENT).to(authOptions);
    this.component(AuthorizationComponent);
    this
      .bind('providers.role-authorization')
      .toProvider(RoleAuthorizationProvider)
      .tag(AuthorizationTags.AUTHORIZER);
  }
}
