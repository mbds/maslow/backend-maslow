export const API_URL = "localhost:8080/rest/";

export const OPENHAB_SERVER_HOST = 'localhost';
export const OPENHAB_SERVER_PORT = 8080;
export const THINGS_RESOURCE = '/rest/things/';
export const ITEMS_RESOURCE = '/rest/items/';
