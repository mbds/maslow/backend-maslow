import { get, param, requestBody, post, getModelSchemaRef } from '@loopback/openapi-v3';
import { OpenhabService } from '../services';
import { inject } from '@loopback/context';
import { authenticate } from '@loopback/authentication';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import { repository } from '@loopback/repository';
import { ThingAccessRepository, ThingRepository, HistoryLogRepository, UserRepository } from '../repositories';
import { HistoryLog } from '../models';

export class ThingController {

  constructor(
    @inject('services.openhab') private openhabService: OpenhabService,
    @repository(ThingAccessRepository) private thingAccessRepository: ThingAccessRepository,
    @repository(UserRepository) private userRepository: UserRepository,
    @repository(ThingRepository) private thingRepository: ThingRepository,
    @repository(HistoryLogRepository) private historyLogRepository: HistoryLogRepository,

  ) { }

  @get('/things')
  @authenticate('jwt')
  async find(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile
  ): Promise<any[]> {
    const allThings = await this.openhabService.getAllThings();
    for (let i = 0; i < allThings.length; i++) {
      let thing = { thing_uid: allThings[i].UID }
      try {
        await this.thingRepository.create(thing);
      } catch (error) {
        console.log('Error while migrating : ' + error);
      }
    }
    const user = await this.userRepository.findById(currentUserProfile.id);
    if (user.roleId === "ADMIN" || user.roleId === "EMERGENCY") {
      return allThings;
    }
    else {

      const thingsAccesses = await this.thingAccessRepository.find({ where: { userId: currentUserProfile.id } });
      //   //Get valables accesses
      const valableAccesses = thingsAccesses.filter(access =>
        new Date(access.end_date) > new Date() && new Date(access.start_date) < new Date());
      const valableAccessesThings = valableAccesses.map(access => access.thing_uid);
      const accessibleThings = allThings.filter(thing => valableAccessesThings.includes(thing.UID));
      return accessibleThings;
    }
  }

  @get('/things/{thingUid}')
  @authenticate('jwt')
  async findByUid(
    @param.path.string('thingUid') thingUid: string
  ): Promise<any> {

    return this.openhabService.getThing(thingUid);
  }

  @get('/items/')
  @authenticate('jwt')
  async findItems(
  ): Promise<any> {
    let allItems = await this.openhabService.getItems();
    let allItemsNames = allItems.map(tap => tap.name);
    console.log("allItemsName", allItemsNames);
    return allItems;
  }

  @get('/items/{itemName}')
  @authenticate('jwt')
  async findItemByName(
    @param.path.string('itemName') itemName: string
  ): Promise<any> {

    return this.openhabService.getItem(itemName);
  }

  @post('/items/{itemName}')
  @authenticate('jwt')
  async updateItem(
    @param.path.string('itemName') itemName: string,
    @requestBody() body: { itemValue: number | OpenhabState, thingUid: string },
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile
  ): Promise<any> {
    let log = new HistoryLog();
    log.userId = currentUserProfile.id;
    log.thingId = body.thingUid;
    log.itemName = itemName;
    log.state = body.itemValue.toString();
    log.creationDate = new Date();
    this.historyLogRepository.create(log);
    return this.openhabService.updateItem(itemName, body.itemValue.toString());
  }
}

export enum OpenhabState {
  ON = 'ON',
  OFF = 'OFF',
  CLOSE = 'CLOSE',
  OPEN = 'OPEN'
}
