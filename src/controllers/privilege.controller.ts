import { repository, Where, Filter, FilterExcludingWhere } from '@loopback/repository';
import { post, param, get, getModelSchemaRef, patch, requestBody, HttpErrors, put, del } from '@loopback/rest';
import { RoleRepository } from '../repositories';
import { inject } from '@loopback/core';
import { TokenServiceBindings } from '../keys';
import { TokenService, authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';
import { PrivilegeRepository } from '../repositories/privilege.repository';
import { Privilege } from '../models/privilege.model';

export class PrivilegeController {
  constructor(
    @repository(PrivilegeRepository) private privilegeRepository: PrivilegeRepository,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,

  ) { }

  @post('/privileges', {
    responses: {
      '200': {
        description: 'Privilege',
        content: {
          'application/json': {
            schema: {
              schema: getModelSchemaRef(Privilege, { exclude: ['id'] }),
            }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async create(@requestBody() privilege: Privilege): Promise<Privilege> {
    let lastId = await this.getLast();
    privilege.id = lastId + 1;
    try {
      const savedPrivilege = await this.privilegeRepository.create(privilege);
      return savedPrivilege;
    } catch (error) {
      // Duplicate keys
      if (error.code === 11000) {
        throw new HttpErrors.Conflict('Conflict error');
      } else {
        throw error;
      }
    }
  }

  @get('/privileges', {
    responses: {
      '200': {
        description: 'Array of Privilege model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Privilege),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  //@authorize({ allowedRoles: ['ADMIN'] })
  async find(
    @param.filter(Privilege) filter?: Filter<Privilege>,
  ): Promise<Privilege[]> {
    const privileges = await this.privilegeRepository.find(filter);
    return privileges;
  }

  @get('/privileges/{id}', {
    responses: {
      '200': {
        description: 'Privilege model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Privilege),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async findById(
    @param.path.string('id') id: number,
    @param.filter(Privilege, { exclude: 'where' }) filter?: FilterExcludingWhere<Privilege>
  ): Promise<Privilege> {
    const findPrivilege = await this.privilegeRepository.findById(id, filter);
    return findPrivilege;
  }

  @put('/privileges/{id}', {
    responses: {
      '204': {
        description: 'Privilege put success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            schema: getModelSchemaRef(Privilege, { partial: true, exclude: ['id'] }),
          }
        }
      }
    })
    privilege: Privilege,
  ): Promise<void> {
    await this.privilegeRepository.updateById(id, privilege);
  }

  @del('/privileges/{id}', {
    responses: {
      '204': {
        description: 'Privilege DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async deleteById(@param.path.string('id') id: number): Promise<void> {
    await this.privilegeRepository.deleteById(id);
  }

  private async getLast() {
    const lastIndexPrivilege = (await this.privilegeRepository.find({ order: ['id DESC'], fields: { id: true } }));
    return lastIndexPrivilege[0].id || 1;
  }
}
