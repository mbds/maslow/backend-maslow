import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository';
import { post, param, get, getModelSchemaRef, patch, put, del, requestBody } from '@loopback/rest';
import { ThingAccess } from '../models';
import { ThingAccessRepository, ThingAccessPrivilegesRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';
import { PrivilegeRepository } from '../repositories/privilege.repository';

export class ThingAccessController {
  constructor(
    @repository(ThingAccessPrivilegesRepository)
    public thingAccessPrivilegesRepository: ThingAccessPrivilegesRepository,
    @repository(ThingAccessRepository)
    public thingAccessRepository: ThingAccessRepository,
    @repository(PrivilegeRepository)
    public privilegeRepository: PrivilegeRepository,
  ) { }


  @post('/thingAccesses', {
    responses: {
      '200': {
        description: 'ThingAccess model instance',
        content: { 'application/json': { schema: getModelSchemaRef(ThingAccess) } },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            'x-ts-type': 'any'
          }
        },
      },
    })
    thingAccess: any,
  ): Promise<ThingAccess> {
    let createThing = new ThingAccess();
    createThing.thing_uid = thingAccess.thing_uid;
    createThing.start_date = thingAccess.start_date;
    createThing.end_date = thingAccess.end_date;
    createThing.userId = thingAccess.userId;
    //creation du thingAccess sans les privileges en premier
    let thingAccessCreated = await this.thingAccessRepository.create(createThing);

    //creation des privileges en deuxieme
    for (let i = 0; i < thingAccess.privileges.length; i++) {
      if (thingAccess.privileges[i] && thingAccess.privileges[i].id != undefined) {
        const res = await this.thingAccessPrivilegesRepository.create({ thingAccessId: thingAccessCreated.id, privilegeId: thingAccess.privileges[i].id });
      }
    }
    return thingAccessCreated;
  }

  @get('/thingAccesses', {
    responses: {
      '200': {
        description: 'Array of ThingAccess model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ThingAccess, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async find(
    @param.filter(ThingAccess) filter?: Filter<ThingAccess>,
  ): Promise<ThingAccess[]> {
    // recupération de tous les thingAccess
    // ainsi la liste des privilèges associés
    const thingAccesses = await this.thingAccessRepository.find(filter);
    for (let i = 0; i < thingAccesses.length; i++) {
      let thingAccessPrivileges = await this.thingAccessPrivilegesRepository.find({ where: { thingAccessId: thingAccesses[i].id } });
      let privilegeIds = thingAccessPrivileges.map(tap => tap.privilegeId);
      thingAccesses[i].privileges = await this.privilegeRepository.find({ where: { id: { inq: privilegeIds } } });

    }
    return thingAccesses;
  }

  @get('/thingAccesses/{id}', {
    responses: {
      '200': {
        description: 'ThingAccess model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ThingAccess, { includeRelations: true }),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async findById(
    @param.path.string('id') id: number,
    @param.filter(ThingAccess, { exclude: 'where' }) filter?: FilterExcludingWhere<ThingAccess>
  ): Promise<ThingAccess> {

    // recupération d'un thingAccess associé à un id
    // ainsi la liste des privilèges qui lui sont associés
    const thingAccess = await this.thingAccessRepository.findById(id, filter);
    let thingAccessPrivileges = await this.thingAccessPrivilegesRepository.find({ where: { thingAccessId: thingAccess.id } });

    let privilegeIds = thingAccessPrivileges.map(tap => tap.privilegeId);
    thingAccess.privileges = await this.privilegeRepository.find({ where: { id: { inq: privilegeIds } } });
    return thingAccess;
  }
  @put('/thingAccesses/{id}', {
    responses: {
      '204': {
        description: 'ThingAccesses put success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ThingAccess, { partial: true, exclude: ['id'] }),
        }
      }
    })
    ThingAccess: ThingAccess,
  ): Promise<void> {
    await this.thingAccessRepository.updateById(id, ThingAccess);
  }

  @del('/thingAccesses/{id}', {
    responses: {
      '204': {
        description: 'ThingAccess DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async deleteById(@param.path.string('id') id: number): Promise<void> {

    await this.thingAccessRepository.deleteById(id);

  }
}
