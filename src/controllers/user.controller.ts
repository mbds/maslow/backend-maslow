import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository';
import { post, param, get, getModelSchemaRef, patch, requestBody, HttpErrors, del, put } from '@loopback/rest';
import { User } from '../models';
import { UserRepository, Credentials, ThingAccessRepository } from '../repositories';
import { promisify } from 'util';
import { inject } from '@loopback/core';
import { TokenServiceBindings, UserServiceBindings, PasswordHasherBindings } from '../keys';
import { MyUserService } from '../services/user.service';
import { CredentialsRequestBody } from './specs/user.controller.specs';
import { PasswordHasher } from '../services/hash-password.service';
import { TokenService, authenticate } from '@loopback/authentication';
import { OPERATION_SECURITY_SPEC } from '../utils/security.specs';
import { SecurityBindings, UserProfile } from '@loopback/security';
import { authorize } from '@loopback/authorization';

export class UserController {
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
    // @repository(UserRoleRepository) private userRoleRepository: UserRoleRepository,
    @repository(ThingAccessRepository) private thingAccessRepository: ThingAccessRepository,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher
  ) { }

  @post('/users', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              schema: getModelSchemaRef(User, { exclude: ['id', 'creationDate', 'modificationDate'] }),
            }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async create(@requestBody() user: any): Promise<User> {
    user.password = await this.passwordHasher.hashPassword(user.password);
    var dateobj = new Date();
    user.creationDate = dateobj.toISOString()
    user.modificationDate = dateobj.toISOString();

    try {
      const savedUser = await this.userRepository.create(user);
      delete savedUser.password;
      return savedUser;
    } catch (error) {

      // Duplicate keys
      if (error.code === 11000) {
        if (error.errmsg.includes('index: uniqueEmail')) {
          throw new HttpErrors.Conflict('Email value is already taken');
        } else {
          throw new HttpErrors.Conflict('Conflict error');
        }
      } else {
        throw error;
      }
    }
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async find(
    @param.filter(User) filter?: Filter<User>,
  ): Promise<User[]> {
    const users = await this.userRepository.find(filter);

    for (let i = 0; i < users.length; i++) {
      users[i].thingsAccesses = await this.thingAccessRepository.find({ where: { userId: users[i].id } });
    }
    return users;
  }

  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, { includeRelations: true }),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async findById(
    @param.path.string('id') id: number,
    @param.filter(User, { exclude: 'where' }) filter?: FilterExcludingWhere<User>
  ): Promise<User> {
    const findUser = await this.userRepository.findById(id, filter);
    //findUser.roles = await this.userRoleRepository.find({ where: { userId: { like: id } } });
    findUser.thingsAccesses = await this.thingAccessRepository.find({ where: { userId: findUser.id } });
    return findUser;
  }

  @put('/users/{id}', {
    responses: {
      '204': {
        description: 'User put success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, { partial: true, exclude: ['id', 'password', 'creationDate', 'modificationDate'] }),
        }
      }
    })
    user: User,
  ): Promise<void> {
    var dateobj = new Date();
    user.modificationDate = dateobj.toISOString();
    await this.userRepository.updateById(id, user);
  }


  @post('users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string'
                }
              }
            }
          }
        }
      }
    }
  })
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials): Promise<{ token: string }> {
    const user = await this.userService.verifyCredentials(credentials);
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);

    return { token };
  }


  /**
   * Update the connected user only
   * @param currentUserProfile
   * @param user
   */
  @get('/users/me', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User PATCH success',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User
            }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async getCurrentUser(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile
  ): Promise<User> {
    // Inclusion not working : Trpuble with mongo ObjectId
    const user = await this.userRepository.findById(currentUserProfile.id, { fields: { password: false } });
    return user;
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async deleteById(@param.path.string('id') id: number): Promise<void> {
    await this.userRepository.deleteById(id);
  }
}
