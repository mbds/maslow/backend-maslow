import { repository, Where, Filter, FilterExcludingWhere } from '@loopback/repository';
import { post, param, get, getModelSchemaRef, patch, requestBody, HttpErrors, put, del } from '@loopback/rest';
import { Role } from '../models';
import { RoleRepository } from '../repositories';
import { inject } from '@loopback/core';
import { TokenServiceBindings } from '../keys';
import { TokenService, authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';

export class RoleController {
  constructor(
    @repository(RoleRepository) private roleRepository: RoleRepository,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,

  ) { }

  @post('/roles', {
    responses: {
      '200': {
        description: 'Role',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': Role
            }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async create(@requestBody() role: any): Promise<Role> {
    try {
      const savedRole = await this.roleRepository.create(role);
      return savedRole;
    } catch (error) {

      // Duplicate keys
      if (error.code === 11000) {

        throw new HttpErrors.Conflict('Conflict error');

      } else {
        throw error;
      }
    }
  }

  @get('/roles', {
    responses: {
      '200': {
        description: 'Array of Role model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Role),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async find(
    @param.filter(Role) filter?: Filter<Role>,
  ): Promise<Role[]> {
    const roles = await this.roleRepository.find(filter);
    return roles;
  }

  @get('/roles/{id}', {
    responses: {
      '200': {
        description: 'Role model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Role),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Role, { exclude: 'where' }) filter?: FilterExcludingWhere<Role>
  ): Promise<Role> {
    const findRole = await this.roleRepository.findById(id, filter);
    return findRole;
  }

  @put('/roles/{id}', {
    responses: {
      '204': {
        description: 'Role put success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Role, { partial: true, exclude: ['id'] }),
        }
      }
    })
    role: Role,
  ): Promise<void> {
    await this.roleRepository.updateById(id, role);
  }

  @del('/roles/{id}', {
    responses: {
      '204': {
        description: 'Role DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.roleRepository.deleteById(id);
  }
}
