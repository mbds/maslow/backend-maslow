import { Filter, repository } from '@loopback/repository';
import { param, get, getModelSchemaRef } from '@loopback/rest';
import { HistoryLog } from '../models';
import { HistoryLogRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';
export class HistoryLogController {
  constructor(
    @repository(HistoryLogRepository) private historyLogRepository: HistoryLogRepository,
  ) { }
  @get('/historiques', {
    responses: {
      '200': {
        description: 'Array of histories model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(HistoryLog),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async find(
    @param.filter(HistoryLog) filter?: Filter<HistoryLog>,
  ): Promise<HistoryLog[]> {
    const historiques = await this.historyLogRepository.find({ order: ['id DESC'] });
    return historiques;
  }
}
