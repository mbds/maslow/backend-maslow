import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  ThingAccess,
} from '../models';
import { ThingAccessRepository } from '../repositories';
import { Privilege } from '../models/privilege.model';

export class ThingAccessPrivilegeController {
  constructor(
    @repository(ThingAccessRepository) protected thingAccessRepository: ThingAccessRepository,
  ) { }

  @get('/thingaccesses/{id}/privileges', {
    responses: {
      '200': {
        description: 'Array of ThingAccess has many Privilege through ThingAccessPrivileges',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Privilege) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: number,
    @param.query.object('filter') filter?: Filter<Privilege>,
  ): Promise<Privilege[]> {
    return this.thingAccessRepository.privileges(id).find(filter);
  }

  @post('/thingaccesses/{id}/privileges', {
    responses: {
      '200': {
        description: 'create a Privilege model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Privilege) } },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof ThingAccess.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Privilege, {
            title: 'NewPrivilegeInThingAccess',
            exclude: ['id'],
          }),
        },
      },
    }) privilege: Omit<Privilege, 'id'>,
  ): Promise<Privilege> {
    return this.thingAccessRepository.privileges(id).create(privilege);
  }

  @patch('/thingaccesses/{id}/privileges', {
    responses: {
      '200': {
        description: 'ThingAccess.Privilege PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Privilege, { partial: true }),
        },
      },
    })
    privilege: Partial<Privilege>,
    @param.query.object('where', getWhereSchemaFor(Privilege)) where?: Where<Privilege>,
  ): Promise<Count> {
    return this.thingAccessRepository.privileges(id).patch(privilege, where);
  }

  @del('/thingaccesses/{id}/privileges', {
    responses: {
      '200': {
        description: 'ThingAccess.Privilege DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.string('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Privilege)) where?: Where<Privilege>,
  ): Promise<Count> {
    return this.thingAccessRepository.privileges(id).delete(where);
  }
}
