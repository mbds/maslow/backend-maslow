import { Entity, model, property } from '@loopback/repository';

@model()
export class HistoryLog extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  id: number;

  @property({
    type: 'number',
    required: true,
  })
  userId: number;

  @property({
    type: 'string',
    required: true,
  })
  thingId: string;

  @property({
    type: 'string',
    required: true,
  })
  itemName: string;

  @property({
    type: 'string',
    required: true,
  })
  state: string;

  @property({
    type: 'date',
    required: true,
  })
  creationDate: Date;

  constructor(data?: Partial<HistoryLog>) {
    super(data);
  }
}

export interface HistoryLogRelations {
  // describe navigational properties here
}

export type HistoryLogWithRelations = HistoryLog & HistoryLogRelations;
