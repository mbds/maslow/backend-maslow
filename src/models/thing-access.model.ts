import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { User } from '.';
import { Time } from './time';
import { Privilege } from './privilege.model';
import { ThingAccessPrivileges } from './thing-access-privileges.model';

@model({
  settings: {
    indexes: {
      uniqueThing_uid: {
        keys: {
          thing_uid: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class ThingAccess extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  thing_uid: string;

  @property({
    type: 'date',
    required: true,
  })
  start_date: string;

  @property({
    type: 'date',
    required: true,
  })
  end_date: string;

  @property({
    type: 'boolean',
  })
  periodic?: boolean;

  @property({
    type: 'string',
  })
  periodicity?: string;

  @property({
  })
  time?: Time;

  @belongsTo(() => User)
  userId: number;

  @hasMany(() => Privilege, { through: { model: () => ThingAccessPrivileges } })
  privileges: Privilege[];

  constructor(data?: Partial<ThingAccess>) {
    super(data);
  }
}

export interface ThingAccessRelations {
  // describe navigational properties here
}

export type ThingAccessWithRelations = ThingAccess & ThingAccessRelations;
