import { Entity, model, property, hasMany, belongsTo } from '@loopback/repository';
import { ThingAccess } from './thing-access.model';
import { Role } from '.';

@model({
  settings: {
    indexes: {
      uniqueEmail: {
        keys: {
          email: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id: number;

  @property({
    type: 'string',
    generated: false,
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  lastname: string;

  @property({
    type: 'string',
    required: true,
  })
  firstname: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'date',
  })
  creationDate: string;

  @property({
    type: 'date',
    required: true,
  })
  modificationDate: string;

  @hasMany(() => ThingAccess, { keyTo: 'userId' })
  thingsAccesses: ThingAccess[];

  @belongsTo(() => Role)
  roleId: string;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
