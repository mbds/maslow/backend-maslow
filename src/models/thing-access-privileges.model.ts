import { Entity, model, property } from '@loopback/repository';

@model()
export class ThingAccessPrivileges extends Entity {
  @property({
    type: 'number',
    required: true,
  })
  thingAccessId: number;

  @property({
    type: 'number',
    required: true,
  })
  privilegeId: number;

  @property({
    type: 'number',
    id: true,
  })
  id: number;


  constructor(data?: Partial<ThingAccessPrivileges>) {
    super(data);
  }
}

export interface ThingAccessPrivilegesRelations {
  // describe navigational properties here
}

export type ThingAccessPrivilegesWithRelations = ThingAccessPrivileges & ThingAccessPrivilegesRelations;
