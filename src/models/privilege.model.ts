import { Entity, model, property } from '@loopback/repository';

@model({
  settings: {
    indexes: {
      uniqueName: {
        keys: {
          name: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class Privilege extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  action: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;


  constructor(data?: Partial<Privilege>) {
    super(data);
  }
}

export interface PrivilegeRelations {
  // describe navigational properties here
}

export type PrivilegeWithRelations = Privilege & PrivilegeRelations;
