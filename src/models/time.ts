import { Entity, model, property, Model } from '@loopback/repository';

@model()
export class Time extends Model {

  @property({
    type: 'string',
    required: true,
  })
  start_hour: string;

  @property({
    type: 'string',
    required: true,
  })
  end_hour: string;


  constructor(data?: Partial<Time>) {
    super(data);
  }
}

export interface TimeRelations {
  // describe navigational properties here
}

export type TimeWithRelations = Time & TimeRelations;
