import { Entity, model, property } from '@loopback/repository';

@model({
  settings: {
    indexes: {
      uniqueThing_uid: {
        keys: {
          thing_uid: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class Thing extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  thing_uid: string;


  constructor(data?: Partial<Thing>) {
    super(data);
  }
}

export interface ThingRelations {
  // describe navigational properties here
}

export type ThingWithRelations = Thing & ThingRelations;
