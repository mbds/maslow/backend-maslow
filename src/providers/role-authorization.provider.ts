import { Provider } from '@loopback/core';
import { Authorizer, AuthorizationContext, AuthorizationMetadata, AuthorizationDecision } from '@loopback/authorization';
import { repository } from '@loopback/repository';
import { UserRepository } from '../repositories';

export class RoleAuthorizationProvider implements Provider<Authorizer> {
  roleRepository: any;
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
  ) { }

  /**
   * @returns authenticateFn
   */
  value(): Authorizer {
    return this.authorize.bind(this);
  }

  async authorize(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    const user = authorizationCtx.principals[0];
    const findUser = await this.userRepository.findById(user.id);
    user.roles = findUser.roleId;
    console.log("user = ", user);
    console.log("finUser:", findUser);
    const allowedRoles = metadata.allowedRoles;
    if (allowedRoles) {
      if (user.roles) {
        if (allowedRoles.includes(user.roles)) {
          return AuthorizationDecision.ALLOW;
        }
      }
      return AuthorizationDecision.DENY;
    } else {
      return AuthorizationDecision.ALLOW;
    }
  }
}
