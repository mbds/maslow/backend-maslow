import { DefaultCrudRepository, HasManyRepositoryFactory, repository, DataObject, Options } from '@loopback/repository';
import { User, UserRelations, ThingAccess } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { ThingAccessRepository } from './thing-access.repository';


export class UserRepository extends DefaultCrudRepository<User, typeof User.prototype.id, UserRelations> {

  public readonly thingsAccesses: HasManyRepositoryFactory<ThingAccess, typeof User.prototype.id>;
  //public readonly roles: HasManyRepositoryFactory<UserRole, typeof User.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @repository.getter('ThingAccessRepository')
    getThingAccessRepository: Getter<ThingAccessRepository>,
    //@repository.getter('UserRoleRepository')
    //getUserRoleRepository: Getter<UserRoleRepository>
  ) {
    super(User, dataSource);

    this.thingsAccesses = this.createHasManyRepositoryFactoryFor('thingsAccesses', getThingAccessRepository);
    //this.roles = this.createHasManyRepositoryFactoryFor('roles', getUserRoleRepository);

    this.registerInclusionResolver('thingsAccesses', this.thingsAccesses.inclusionResolver);
    //this.registerInclusionResolver('roles', this.roles.inclusionResolver);
  }

  async create(entity: DataObject<User>, options?: Options): Promise<User> {
    entity.id = await this.getLast() + 1;
    return super.create(entity);
  }

  private async getLast() {
    const lastEntity = (await this.find({ order: ['id DESC'], limit: 1, fields: { id: true } }));
    return lastEntity.length !== 0 ? lastEntity[0].id : 0;
  }
}


export type Credentials = {
  email: string;
  password: string;
};
