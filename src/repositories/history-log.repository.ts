import { DefaultCrudRepository, DataObject, Options } from '@loopback/repository';
import { HistoryLog, HistoryLogRelations } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class HistoryLogRepository extends DefaultCrudRepository<
  HistoryLog,
  typeof HistoryLog.prototype.id,
  HistoryLogRelations
  > {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(HistoryLog, dataSource);
  }

  async create(entity: DataObject<HistoryLog>, options?: Options): Promise<HistoryLog> {
    entity.id = await this.getLast() + 1;
    return super.create(entity);
  }

  private async getLast() {
    const lastEntity = (await this.find({ order: ['id DESC'], limit: 1, fields: { id: true } }));
    return lastEntity.length !== 0 ? lastEntity[0].id : 0;
  }
}
