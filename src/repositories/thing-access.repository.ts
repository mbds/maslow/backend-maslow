import { DefaultCrudRepository, BelongsToAccessor, repository, HasManyThroughRepositoryFactory, DataObject, Options } from '@loopback/repository';
import { ThingAccess, ThingAccessRelations, User } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { UserRepository } from '.';
import { ThingAccessPrivilegesRepository } from './thing-access-privileges.repository';
import { PrivilegeRepository } from './privilege.repository';
import { Privilege } from '../models/privilege.model';
import { ThingAccessPrivileges } from '../models/thing-access-privileges.model';

export class ThingAccessRepository extends DefaultCrudRepository<ThingAccess, typeof ThingAccess.prototype.id, ThingAccessRelations> {


  public readonly user: BelongsToAccessor<User, typeof ThingAccess.prototype.id>;

  public readonly privileges: HasManyThroughRepositoryFactory<Privilege, typeof Privilege.prototype.id,
    ThingAccessPrivileges,
    typeof ThingAccess.prototype.id
  >;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @repository.getter('UserRepository')
    getUserRepository: Getter<UserRepository>,
    @repository.getter('ThingAccessPrivilegesRepository') protected thingAccessPrivilegesRepositoryGetter: Getter<ThingAccessPrivilegesRepository>,
    @repository.getter('PrivilegeRepository') protected privilegeRepositoryGetter: Getter<PrivilegeRepository>,
  ) {
    super(ThingAccess, dataSource);
    this.privileges = this.createHasManyThroughRepositoryFactoryFor('privileges', privilegeRepositoryGetter, thingAccessPrivilegesRepositoryGetter,);
    this.user = this.createBelongsToAccessorFor('user', getUserRepository);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }

  async create(entity: DataObject<ThingAccess>, options?: Options): Promise<ThingAccess> {
    entity.id = await this.getLast() + 1;
    return super.create(entity);
  }

  private async getLast() {
    const lastEntity = (await this.find({ order: ['id DESC'], limit: 1, fields: { id: true } }));
    return lastEntity.length !== 0 ? lastEntity[0].id : 0;
  }
}
