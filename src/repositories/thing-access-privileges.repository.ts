import { DefaultCrudRepository, DataObject, Options } from '@loopback/repository';
import { ThingAccessPrivileges, ThingAccessPrivilegesRelations } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class ThingAccessPrivilegesRepository extends DefaultCrudRepository<
  ThingAccessPrivileges,
  typeof ThingAccessPrivileges.prototype.id,
  ThingAccessPrivilegesRelations
  > {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(ThingAccessPrivileges, dataSource);
  }
  async create(entity: DataObject<ThingAccessPrivileges>, options?: Options): Promise<ThingAccessPrivileges> {
    entity.id = await this.getLast() + 1;
    return super.create(entity);
  }

  private async getLast() {
    const lastEntity = (await this.find({ order: ['id DESC'], limit: 1, fields: { id: true } }));
    return lastEntity.length !== 0 ? lastEntity[0].id : 0;
  }
}
