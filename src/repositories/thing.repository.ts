import { DefaultCrudRepository, DataObject, Options } from '@loopback/repository';
import { Thing, ThingRelations } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class ThingRepository extends DefaultCrudRepository<
  Thing,
  typeof Thing.prototype.id,
  ThingRelations
  > {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(Thing, dataSource);
  }

  async create(entity: DataObject<Thing>, options?: Options): Promise<Thing> {
    entity.id = await this.getLast() + 1;
    return super.create(entity);
  }

  private async getLast() {
    const lastEntity = (await this.find({ order: ['id DESC'], limit: 1, fields: { id: true } }));
    return lastEntity.length !== 0 ? lastEntity[0].id : 0;
  }
}
