import { DefaultCrudRepository, DataObject, Options } from '@loopback/repository';
import { Role, RoleRelations } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject } from '@loopback/core';
import { Privilege, PrivilegeRelations } from '../models/privilege.model';

export class PrivilegeRepository extends DefaultCrudRepository<
  Privilege,
  typeof Privilege.prototype.id,
  PrivilegeRelations
  > {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(Privilege, dataSource);
  }

  async create(entity: DataObject<Privilege>, options?: Options): Promise<Privilege> {
    entity.id = await this.getLast() + 1;
    return super.create(entity);
  }

  private async getLast() {
    const lastEntity = (await this.find({ order: ['id DESC'], limit: 1, fields: { id: true } }));
    return lastEntity.length !== 0 ? lastEntity[0].id : 0;
  }
}
