import { DefaultCrudRepository } from '@loopback/repository';
import { MongodbDataSource } from '../datasources';
import { inject } from '@loopback/core';
import { Lock, LockRelations } from '../models/lock.model';

export class LockRepository extends DefaultCrudRepository<
  Lock,
  typeof Lock.prototype.id,
  LockRelations
  > {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(Lock, dataSource);
  }
}
